﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    public Transform Target;
    public int count;
    public bool isMoveable,isReverse;
    private void Start()
    {
        this.gameObject.name = this.gameObject.name + " " + System.DateTime.Now.ToString("MM/dd/yyyy");
        Moveabletrue();              
    }    
    private void Update()
    {
        //this.gameObject.name = this.gameObject.name + " " + System.DateTime.Now.ToString("MM/dd/yyyy");
        
        if (isMoveable)
        {            
            this.transform.position = Vector3.MoveTowards(this.transform.position, Target.transform.position, 2f * Time.deltaTime);  
            if(Vector3.Distance(this.transform.position,Target.transform.position) <= 0.1f)
            {
                isMoveable = false;
                if(Target == CubeSpawnerScript.css.PatrolPoint2)
                {
                    Reverse();
                    Invoke("Moveabletrue", 3f);

                }
                else
                {
                    Reverse();
                    Moveabletrue();
                }
            }           
        }
    }

    public void Reverse()
    {
        if (count == 3)
        {
            isReverse = true;
        }
        if (count == 1)
        {
            isReverse = false;
        }
    }

    public void Moveabletrue()
    {
        isMoveable = true;
        if (isReverse)
        {
            count--;
        }
        else
        {
            count++;
        }
        if (count == 1)
        {
            Target = CubeSpawnerScript.css.PatrolPoint1;
        }
        else if (count == 2)
        {
            Target = CubeSpawnerScript.css.PatrolPoint2;            
        }
        else if (count == 3)
        {
            Target = CubeSpawnerScript.css.PatrolPoint3;            
        }
    }
}
