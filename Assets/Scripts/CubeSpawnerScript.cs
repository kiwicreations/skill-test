﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawnerScript : MonoBehaviour
{
    
    public Transform PatrolPoint1, PatrolPoint2, PatrolPoint3;
    public GameObject Prefab,Cube;
    public static CubeSpawnerScript css;
    int count;
    private void Awake()
    {
        css = this;
    }
    void Update()
    {
        
    }
    public void Spawner()
    {
        Cube = Instantiate(Prefab, PatrolPoint1.position,Quaternion.identity);

    }
    public void Pause()
    {
        if(Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
